/**
 * Created by Joseph Santos on 12/25/2016.
 */

var fdsTimer = function(){

    // our auth token
    this.authToken = '0/df20228a6819b65f3bc4a2449f5ff0df';

    // our api endpoint url
    this.endpointUrl = 'https://asanatimer.freshdesignstudio.com';

    // the currently logged in user
    this.user = null;

    // current task ID we're working on
    this.taskId = null;

    // flag if timer is running on the current task
    this.isTimerRunning = false;

    // placeholder for new task ID. This gets updated whenever we click on a task
    this.newTaskId = null;

    // our port object
    this.port = null;

    /**
     * self invoking bootstrap function that defines mutator observers
     */
    this.init = function(){

        var self = this;

        //set our click event for the timer
        $(document).on('click', '.fds-timer', function(){

            if(self.isTimerRunning){
                self.stopTimer();
            } else {
                self.startTimer();
            }

        });

        // When Asana finishes loading (by detecting a mutation on the #center_pane__contents element), we load our app's views
        ready('#center_pane .gridPaneView', function(element) {

            // let's grab the currently logged in user (should be you!)
            self.getUser();

        });

        // Whenever the right pane container changes, we update our app's view
        ready('#right_pane_container #details_pane_header .reskinToolbarViewsWithShortcuts', function(element) {
            self.updateView();
        });

    };

    /**
     * Gets the currently logged in user from Asana API
     *
     * @param callbackfn
     */
    this.getUser = function(callbackfn){

        var that = this;

        $.ajax({
            url: "https://app.asana.com/api/1.0/users/me",
            dataType: 'jsonp',
            beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ that.authToken )},
            complete: function (resp) {

                // our return data
                var data = $.parseJSON(resp.responseText);

                // set our user data
                that.user = data.data;

                // connect to port and add listener
                that.port = chrome.runtime.connect({name: "fds-asana-timer"});
                that.port.onMessage.addListener(function(msg) {

                    console.log("received message from extension:");
                    console.log(msg);

                    // if the message is to update our open tasks, run our "updateOpenTasks function
                    if (msg.request == "update-open-tasks") {
                        that.updateOpenTasks(msg.data);
                    }
                });

                // post a message to get open tasks
                that.port.postMessage({request: "get-open-tasks"});

            }
        });

    };

    /**
     * This function updates our view/app whenever the right pane changes
     */
    this.updateView = function(){

        // we update the selected task id
        var theId = $('.grid-row-selected').attr('id');
        this.newTaskId = theId.replace("item_row_view_", "");

        // add a timer button
        this.addTimerButton();

        // get our timer history
        this.getTimerHistory(this.newTaskId);

    };

    /**
     * Pings the server for any open tasks. This function runs every minute.
     * setInterval function that defines the loop set in the init() function
     *
     * @param obj
     * @param dat
     */
    this.updateOpenTasks = function(data){

        console.log("updating open tasks with data:");
        console.log(data);

        var self = this;

        for(var n in data.data){

            var theData = data.data[n];

            // if the task belongs to you, the class name is 'primary', else it's 'secondary'
            var isTaskMine = theData.userID == self.user.id ? true : false;
            var myclassName = isTaskMine ? 'primary' : 'secondary';
            
            var userID = theData.userID;
            var username = theData.username;
            var userImage = theData.userimage;

            // if the user doesn't have a timer icon, add it
            if($('.fds-timer-grid.user-'+ userID).length == 0) {

                if(isTaskMine) {

                    // change the current task ID to the return data ID
                    self.taskId = theData.taskID;

                    // set our flag to true
                    self.isTimerRunning = true;

                    if(self.taskId == self.newTaskId) {
                        // add "active" css to button
                        $('.fds-timer #property_fds').addClass('active');
                    }

                }

                self.addListIcon(theData.taskID, myclassName, userID, username, userImage);
            }
            // if the user has an icon and it's a different task
            else if($('.fds-timer-grid.user-'+ userID).length > 0 && $('.fds-timer-grid.task-'+ theData.taskID + '.user-' + userID).length == 0) {

                // remove the current timer icon
                $('.fds-timer-grid.user-'+ userID).remove();

                // add the new timer icon
                self.addListIcon(theData.taskID, myclassName, userID, username, userImage);

            }

        }

        // if there are no open tasks, remove all grid icons
        if(data.length == 0) {
            $('.fds-timer-grid').remove();
        }
    };

    /**
     * Starts the timer
     */
    this.startTimer = function(){

        // change the current task ID to the new task ID
        this.taskId = this.newTaskId;

        // set our flag to true
        this.isTimerRunning = true;

        // add "active" css to button
        $('.fds-timer #property_fds').addClass('active');


        // add a timer icon to the grid
        this.addListIcon(this.taskId, 'primary', this.user.id, this.user.name, this.user.photo.image_27x27);

        // send start time request to server
        this.postStartTime(this.taskId);
    };

    /**
     * Stops the timer
     */
    this.stopTimer = function(){

        // set our flag to false
        this.isTimerRunning = false;

        // remove "active" css to  button
        $('.fds-timer #property_fds').removeClass('active');

        $('.fds-timer-grid.user-'+ this.user.id).remove();
        //this.postTimer(this.taskId, "stopped the timer.");

        // send end time request to server
        this.postEndTime(this.taskId);

        // if the new Task ID is not equal to the current task ID,
        // then we stop the timer for the current task ID and start
        // the timer for the new Task ID
        if(this.newTaskId != this.taskId) {
            this.startTimer();
        }

    };

    /**
     * Adds icons to the list view for any currently running tasks
     *
     * @param taskId
     * @param className
     * @param userId
     * @param username
     * @param userimage
     */
    this.addListIcon = function(taskId, className, userId, username, userimage) {

        var template = '<div class="fds-timer-grid task-'+ taskId +' user-'+ userId +'"><div class="circularButtonView '+ className +'" style="background-image: url('+ userimage +')"></div>';
        $('#item_row_view_'+ taskId +' .grid-tags-and-date').prepend(template);

    };

    /**
     * Sends start time signal to the server
     *
     * @param taskId
     */
    this.postStartTime = function(taskId){

        var data = {
            type : 'starttime',
            taskid : taskId,
            userid : this.user.id,
            username : this.user.name,
            userimage : this.user.photo.image_27x27
        };

        var that = this;
        $.ajax({
            url: that.endpointUrl,
            type: 'POST',
            data:data,
            beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ that.authToken )},
            complete: function (resp) {
                var data = $.parseJSON(resp.responseText);

                // update the time history
                that.getTimerHistory(that.newTaskId);
            }

        });

    };

    /**
     * Sends stop time signal to the server
     *
     * @param taskId
     */
    this.postEndTime = function(taskId){

        var data = {
            type : 'endtime',
            taskid : taskId,
            userid : this.user.id
        }

        var that = this;
        $.ajax({
            url: that.endpointUrl,
            type: 'POST',
            data:data,
            beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ that.authToken )},
            complete: function (resp) {
                var data = $.parseJSON(resp.responseText);

                // update the time history
                that.getTimerHistory(that.newTaskId);
            }
        });
    };


    /**
     * Adds a timer button to the right pane menu. The click event is set during the init() call.
     */
    this.addTimerButton = function(){

        var activeCSS = this.newTaskId == this.taskId && this.isTimerRunning ? "active": "";

        // add a timer button
        $('#details_pane_header .heart-button-container').before('' +
            '<div class="fds-timer">' +
            '<div id="property_fds" class="circularButtonView '+ activeCSS +'">' +
            '<span class="circularButtonView-label">' +
            '<svg class="svgIcon " viewBox="0 0 32 32" title="start tracking"><rect x="18.2" y="10.053" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 22.1741 39.1703)" width="2" height="9.879"></rect><rect x="12" width="8" height="2"></rect><circle cx="16" cy="9" r="1"></circle><circle cx="16" cy="27" r="1"></circle><circle cx="9.636" cy="11.636" r="1"></circle><circle cx="22.364" cy="24.364" r="1"></circle><circle cx="7" cy="18" r="1"></circle><circle cx="25" cy="18" r="1"></circle><circle cx="9.636" cy="24.364" r="1"></circle><path d="M29.707,8.121c0.391-0.391,0.391-1.024,0-1.414l-1.414-1.414c-0.391-0.391-1.024-0.391-1.414,0L24.942,7.23 C22.516,5.214,19.4,4,16,4S9.484,5.214,7.058,7.23L5.121,5.293c-0.391-0.391-1.024-0.391-1.414,0L2.293,6.707 c-0.391,0.391-0.391,1.024,0,1.414l2.08,2.08C2.875,12.431,2,15.113,2,18c0,7.732,6.268,14,14,14s14-6.268,14-14 c0-2.887-0.875-5.569-2.373-7.798L29.707,8.121z M16,30C9.383,30,4,24.617,4,18S9.383,6,16,6s12,5.383,12,12S22.617,30,16,30z"></path></svg>' +
            '</span>' +
            '</div><span></span>' +
            '</div>'
        );
    };

    /**
     * Pings the server for the timer history
     *
     * @param taskId
     * @param callbackfn
     */
    this.getTimerHistory = function(taskId, callbackfn) {

        var that = this;

        var data = {
            type : 'gettimerhistory',
            taskid : taskId
        }

        var that = this;
        $.ajax({
            url: that.endpointUrl,
            type: 'GET',
            data:data,
            beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ that.authToken )},
            complete: function (resp) {
                var data = $.parseJSON(resp.responseText);
                that.addTimerHistory(data);
            }
        });


    };

    this.addTimerHistory = function(data){

        if(data.data.length == 0)
            return;

        var template =
            '<div class="time-history">' +
            '   <div class="feedView flexbox-flexes-in-row taskFeedView-feed">' +
            '       <div class="total-time">Time: '+ data.elapsed.hours +' hr, '+ data.elapsed.minutes +' min</div>' +
            '       <div class="activity-feed">' +
            '           <div class="small-feed-story-group">';

        for(var n in data.data) {

            var theData = data.data[n];

            // start time template
            template += this.getTimeTemplate(theData.userID, theData.username, theData.starttime, "started the timer.");

            // end time template
            if(theData.endtime !== null) {
                template += this.getTimeTemplate(theData.userID, theData.username, theData.endtime, "stopped the timer.");
            }
        }

        template +=
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '</div>';

        if($('#right_pane__contents #time-history').length > 0 ) {
            $('#right_pane__contents #time-history').html(template);
        }
        else{
            $('#right_pane__contents .taskAttachmentsView').after('<div id="time-history">'+template+'</div>');
        }
    };

    this.getTimeTemplate = function(userID, username, theTime, msg) {

        // start time template
        var template =
                '<div class="feed-story">' +
                '   <div class="comment-content">' +
                '       <span class="feed-story-creator">' +
                '           <a href="https://app.asana.com/0/' + userID + '/' + userID +'" class="storyCreatorWidget-creatorLink">'+ username +'</a>' +
                '       </span>' +
                '       <span class="comment-text"><span><span style="white-space:pre-wrap;">'+ msg +'</span></span></span>' +
                '       <span class="inline-timestamp">'+ theTime +'</span>' +
                '   </div>' +
                '</div>'
            ;

        return template;
    };

}

// run our app
var myTimer = new fdsTimer();
myTimer.init();
