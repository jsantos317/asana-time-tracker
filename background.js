/**
 * Created by Joseph Santos on 1/1/2017.
 */
var fdsBackgroundApp = function() {

    // our auth token
    this.authToken = '0/df20228a6819b65f3bc4a2449f5ff0df';

    // our api endpoint url
    this.endpointUrl = 'https://asanatimer.freshdesignstudio.com';

    // our getOpenTasks() interval, in seconds
    this.getOpenTasksInterval = 60;

    this.openTasks = {};

    this.port = null;

    /**
     * Pings the server for any open tasks. This function runs every minute.
     * setInterval function that defines the loop set in the init() function
     *
     * @param obj
     * @param dat
     */
    this.getOpenTasks = function(){

        var self = this;

        var data = {
            type : 'gettimerhistory',
            options : 'getopentimes'
        };

        $.ajax({
            url: self.endpointUrl,
            type: 'GET',
            data: data,
            beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ self.authToken )},
            success: function(data, status) {

                var data = $.parseJSON(data);
                self.openTasks = data;

                // send the data to our extension pages
                if(self.port) {
                    self.port.postMessage({request: "update-open-tasks", data: data});
                }

                // update our browser icon
                self.updateBrowserIcon();

            }
        });
    }

    this.init = function(){

        var self = this;

        // add our onConnect listener
        chrome.runtime.onConnect.addListener(function(port) {

            if(port.name == 'fds-asana-timer') {
                self.port = port;
            }

            // add our message listener
            port.onMessage.addListener(function(msg) {

                if (msg.request == "get-open-tasks") {
                    port.postMessage({request: "update-open-tasks", data: self.openTasks});
                }
            });
        });

        // set getOpenTasks function to run every minute
        self.getOpenTasks();
        setInterval(function () {self.getOpenTasks()}, self.getOpenTasksInterval * 1000);

    };

    this.updateBrowserIcon = function(){

        var onOrOff = this.openTasks.data.length > 0 ? "on" : "off";
        var badgeText = this.openTasks.data.length > 0 ? "" + this.openTasks.data.length : "";

        chrome.browserAction.setIcon({
            path : {
                "19": "images/clock-"+ onOrOff +"-19.png",
                "38": "images/clock-"+ onOrOff +"-38.png"
            }
        });

        chrome.browserAction.setBadgeText({ text: ""+ badgeText });
        chrome.browserAction.setBadgeBackgroundColor({ color: "#3D3D3D" });
    }

};

var myFdsBackgroundApp = new fdsBackgroundApp();
myFdsBackgroundApp.init();