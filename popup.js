/**
 * Created by jsantos317 on 1/2/2017.
 */
var fdsPopupApp = function() {

    var port = null;

    this.init = function(){

        console.log("initializing popup");
        var self = this;

        // connect to port and add listener
        this.port = chrome.runtime.connect({name: "fds-asana-timer"});
        this.port.onMessage.addListener(function(msg) {

            console.log("received message from extension:");
            console.log(msg);

            // if the message is to update our open tasks, run our "updateOpenTasks function
            if (msg.request == "update-open-tasks") {
                self.updateOpenTasks(msg.data);
            }
        });

        // send message to update open tasks
        this.port.postMessage({request : "get-open-tasks"});

    }

    this.updateOpenTasks = function(data){

        if(data.data.length == 0) {
            $('.timers').html('<h1 class="active">There are no active timers running</h1>');
        }
        else {
            var template = '';

            for (var n in data.data) {

                var theData = data.data[n];

                console.log(theData);

                template += '<li class="fds-timer-grid"><div class="circularButtonView primary" style="background-image: url('+ theData.userimage +')">'+ theData.username+' is working on </div></li>';
            }
            $('.timers').html(template);
        }
    }

}

// initialize our app
var myFdsPopupApp = new fdsPopupApp();
myFdsPopupApp.init();